<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>My theme</title>
        <?php wp_head() ?>
        <style>
            .showcase {
                background:url(<?php echo get_theme_mod('showcase_image', get_bloginfo( 'template_url' ).'/img/showcase.jpg'); ?>) no-repeat center center;
            }
        </style>
    </head>

    <?php 
        if ( is_home() ):
            $classes = array( 'home-class', 'class' ); 
        else:
            $classes = array( 'no-class' );
        endif;
    ?>

    <body <?php body_class( $classes ); ?>>
        <nav  class="nav-mytheme navbar navbar-expand-lg navbar-light navbar-mytheme">
            <div class="container">
            
                <a class="navbar-brand" href="#">Navbar w/ text</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">       
                    <?php 
                        wp_nav_menu( 
                            array(
                                'theme_location' => 'primary',
                                'menu_id'        => 'primary-menu',
                                'container'      => false,
                                'depth'          => 2,
                                'menu_class'     => 'navbar-nav ml-auto',
                                'walker'         => new Bootstrap_NavWalker(),
                                'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
                            ) 
                        );
                    ?>
                    
                    
                </div>
            </div>

        </nav>

        <section class="showcase">
           <div class="container text-center">
               <h1><?php echo get_theme_mod('showcase_heading', 'Lorem ipsum dolor') ?></h1>
               <p><?php echo get_theme_mod('showcase_text', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus') ?></p>
               <a href="<?php echo get_theme_mod('btn_url', '#') ?>"class="btn btn-primary"><?php echo get_theme_mod('btn_text', 'Read more') ?></a>
           </div>
        </section>

        <section class="boxes">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <?php if (is_active_sidebar( 'box1' )) : ?>
                            <?php dynamic_sidebar( 'box1' ) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (is_active_sidebar( 'box2' )) : ?>
                            <?php dynamic_sidebar( 'box2' ) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">  
                        <?php if (is_active_sidebar( 'box3' )) : ?>
                            <?php dynamic_sidebar( 'box3' ) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>

