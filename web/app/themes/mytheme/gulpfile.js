
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-css');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('css', function(){
  return gulp.src('assets/sass/*.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('css'))
});

gulp.task('js', function(){
  return gulp.src('client/javascript/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
});

gulp.task('default', [  'css', 'js' ]);
