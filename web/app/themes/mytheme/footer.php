                
                <?php //wp_nav_menu( array('theme_location' => 'secondary') ) ?>
                    <?php if (!is_front_page()) : ?>
                            <div class="col-sm-3  mt-4 sidebar">
                                <?php 
                                    if (is_active_sidebar( 'sidebar' )):
                                        dynamic_sidebar( 'sidebar' );
                                    endif;
                                ?>
                            </div>
                        </div><!-- row -->
                    <?php endif; ?>
                </div> <!-- container -->
            <?php wp_footer(); ?>
        </div>
    </body>
</html>