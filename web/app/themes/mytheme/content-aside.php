<small>Posted on <?php the_time('F j, Y') ?> in <?php the_category() ?></small>
<h3><?php the_title(); ?></h3>
<p><?php the_content(); ?></p>
<h3><?php the_author() ?></h3>
<hr>