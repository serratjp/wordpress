<div class="mt-4">
    <div class="thumbnail-img mr-4" ><?php the_post_thumbnail(); ?></div>
    <small id='post-category'><?php the_category('title'); ?></small>
    <div class="title">
        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>       
    </div>
    <small><?php the_author(); ?></small>   
</div>


