<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>My theme</title>
        <?php wp_head() ?>
    </head>

    <?php 
        if ( is_home() ):
            $classes = array( 'home-class', 'class' ); 
        else:
            $classes = array( 'no-class' );
        endif;
    ?>

    <body <?php body_class( $classes ); ?>>
        <nav  class="nav-mytheme navbar navbar-expand-lg navbar-light navbar-mytheme">
            <div class="container">
            
                <a class="navbar-brand" href="#">Navbar w/ text</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">       
                    <?php 
                        wp_nav_menu( 
                            array(
                                'theme_location' => 'primary',
                                'menu_id'        => 'primary-menu',
                                'container'      => false,
                                'depth'          => 2,
                                'menu_class'     => 'navbar-nav ml-auto',
                                'walker'         => new Bootstrap_NavWalker(),
                                'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
                            ) 
                        );
                    ?>
                    
                    
                </div>
            </div>

        </nav>
        <div class="container">
            <img src="<?php header_image(); ?>" width= "<?php get_custom_header()->width; ?>" heigth="<?php get_custom_header()->heigth; ?>" alt="">