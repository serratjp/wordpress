<?php 
/*
    Scripts
*/
function mytheme_script_enqueue() {
    //css

    wp_enqueue_style( 'custom style', get_template_directory_uri() . '/css/mytheme.css', array(), '1.0.0', 'all');

    //js

    wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/mytheme.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'mytheme_script_enqueue');


//Menus


function mytheme_setup() {

    add_theme_support( 'menus' );

    register_nav_menu( 'primary', 'Primary Header Navigation' );
    register_nav_menu( 'secondary', 'Footer navigation' );
}

add_action( 'init', 'mytheme_setup'  );


//Theme support


add_theme_support( 'custom-background' );
add_theme_support( 'custom-header' );
add_theme_support( 'post-thumbnails' );

add_theme_support( 'post-formats', ['aside', 'image', 'video'] );


//Bootstrap nav

    
require get_template_directory() . '/bootstrap-navwalker.php';


//Location widgets


function wpb_init_widgets($id) {
    register_sidebar(array(
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="sidebar-module">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));

    register_sidebar(array(
        'name' => 'Box1',
        'id' => 'box1',
        'before_widget' => '<div class="box">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));

    register_sidebar(array(
        'name' => 'Box2',
        'id' => 'box2',
        'before_widget' => '<div class="box">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));

    register_sidebar(array(
        'name' => 'Box3',
        'id' => 'box3',
        'before_widget' => '<div class="box">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
}

add_action('widgets_init', 'wpb_init_widgets');

//Customizer File

require get_template_directory(). '/inc/customizer.php';