<?php get_header() ?>
    <div class="row">
        <div class="content-center col-sm-7 mt-4 ml-4 post">
            <?php 
                if (have_posts()):
                    while (have_posts()): the_post(); 
            ?>
                        <h1><?php the_title(); ?></h1> 
                        <div class="thumbnail-img mr-4" ><?php the_post_thumbnail(); ?></div>
                        <small id='post-category'><?php the_category('title'); ?></small> 
                        <?php the_content() ?>
                        <?php comments_template() ?>
            <?php 
                    endwhile;
                endif;
             ?>
        </div>
        <div class="col-sm-3 sidebar">
            <?php 
                if (is_active_sidebar( 'sidebar' )):
                    dynamic_sidebar( 'sidebar' );
                endif;
            ?>
        </div>
    </div>
<?php get_footer() ?>
